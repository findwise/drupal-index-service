<?php

/**
 * @file
 * Admin forms and pages for the fw_indexsvc module.
 */

/**
 * Findwise Index Service administration page. Display a list of existing collections.
 */
function fw_indexsvc_admin_page() {
  $collections = fw_indexsvc_get_collections(TRUE, TRUE);
  return theme('fw_indexsvc_admin_page', array('collections' => $collections));
}

/**
 * Theme the output for the main indentifier administration page.
 */
function theme_fw_indexsvc_admin_page($variables) {
  $collections = $variables['collections'];
  $output = '<p>' . t('This page lists all the <em>collections</em> that are
    currently defined on this system.', array(
      '@add-url' => url(FW_INDEXSVC_ADMIN_PATH . '/add'))) . '</p>';
  // Build out the list of normal, database listed collections.
  $rows = array();
  foreach ($collections as $collection) {
    $ops = theme('links', array(
      'links' => array(
        'collection_clear' => array(
          'title' => t('Clear'),
          'href' => FW_INDEXSVC_ADMIN_PATH . '/manage/'
          . $collection->name . '/clear',
          'attributes' => array('class' => array('btn', 'btn-danger', 'btn-xs', 'btn-clear',
            $collection->status->state != 'success' ? ' disabled' : '')),
        ),
        'collection_reindex' => array(
          'title' => t('Upload'),
          'href' => FW_INDEXSVC_ADMIN_PATH . '/manage/'
          . $collection->name . '/reindex',
          'attributes' => array('class' => array('btn', 'btn-warning', 'btn-xs', 'btn-upload',
            $collection->status->state != 'success' ? ' disabled' : '')),
        ),
        'collection_edit' => array(
          'title' => t('Edit'),
          'href' => FW_INDEXSVC_ADMIN_PATH . '/manage/'
          . $collection->name . '/edit',
          'attributes' => array('class' => array('btn', 'btn-default', 'btn-xs', 'btn-edit')),
        ),
        'collection_delete' => array(
          'title' => t('Delete'),
          'href' => FW_INDEXSVC_ADMIN_PATH . '/manage/'
          . $collection->name . '/delete',
          'attributes' => array('class' => array('btn', 'btn-danger', 'btn-xs', 'btn-delete')),
        ),
      ),
    ));
    $all_sources = "";
    foreach ($collection->sources as $type => $sources) {
      if (!empty($sources)) {
        $all_sources .= '<strong>' . $type . "</strong>(" . implode($sources, ', ') . ')<br />';
      }
    }
    $rows[] = array(
      'data' => array(
        '<strong>' . $collection->name . '</strong>',
        theme('fw_indexsvc_status', array('status' => $collection->status)),
        is_object($collection->status->count) ? t("@remote_count/@local_count", array(
          "@remote_count" => $collection->status->count->remote,
          "@local_count" => $collection->status->count->local,
        )) : '',
        $all_sources,
        $ops,
      ),
    );
  }
  if (!$collections) {
    $rows[] = array(
      array('data' => t('No collections are currently defined.'), 'colspan' => 6),
    );
  }

  $header = array(t('Name'), t('Status'), t('Uploaded'), t('Sources'), t('Operations'));
  $output .= theme('table', array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'class' => array('indexsvc'),
    )
  ));

  return $output;
}

/**
 * Theming of the status field.
 */
function theme_fw_indexsvc_status($variables) {
  $status = $variables['status'];
  // Add bootstrap buttons css.
  drupal_add_css(drupal_get_path('module', 'fw_indexsvc') . '/fw_indexsvc.css', 'file');
  $attributes = array(
    'class' => 'btn btn-xs',
  );
  $attributes['class'] .= ' btn-' . $status->state;
  $attributes['title'] = $status->message;
  return l($status->title, $status->path, array(
    'attributes' => $attributes,
  ));
}

/**
 * Present a form for creating a new collection.
 */
function fw_indexsvc_collection_form($form, &$form_state, $collection = NULL) {
  global $base_url;

  if (!isset($collection)) {
    // Adding a new collection.
    drupal_set_title(t('Add collection'));
  } else {
    // Editing an existing collection.
    $form['iid'] = array(
      '#type' => 'hidden',
      '#value' => $collection->iid,
    );
    drupal_set_title(t('Edit @title', array('@title' => $collection->name)));
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Collection name'),
    '#description' => t('The machine-name for this collection. It may be up to 32 characters long and my only contain letters, dashes, underscores, and trailing numbers. It will be used in URLs and in all API calls.'),
    '#maxlength' => 32,
    '#default_value' => isset($collection) ? $collection->name : '',
    '#required' => TRUE,
  );

  $form['endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint URL'),
    '#description' => t('The full URL to the endpoint. Example: http://192.168.1.1:8080'),
    '#maxlength' => 225,
    '#default_value' => isset($collection) ? $collection->endpoint : '',
    '#required' => TRUE,
  );

  $form['sources'] = array(
    '#type' => 'item',
    '#title' => t('Sources'),
    '#description' => t('The types of content this collection will index.'),
  );

  // Add options for each type.
  $options = array();
  foreach (fw_indexsvc_fetch_type_options() as $type => $type_options) {
    foreach ($type_options as $option => $info) {
      $options[$type][$option] = $info->title . '<div class="description">' . $info->description . '</div>';
    }
  }

  // Add settings for each type.
  $settings = array();
  foreach (fw_indexsvc_fetch_type_settings(isset($collection, $collection->settings) ?
      $collection->settings : array()) as $type => $type_options) {
    foreach ($type_options as $option => $info) {
      $settings[$type][$option] = $info;
    }
  }

  // Build the fieldsets for all types.
  foreach (fw_indexsvc_fetch_type_definitions() as $type => $info) {
    if (!empty($options[$type])) {
      $form['sources'][$type] = array(
        '#type' => 'fieldset',
        '#title' => $info['title'],
        '#description' => $info['description'],
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      $form['sources'][$type]['options'] = array(
        '#type' => 'checkboxes',
        '#options' => $options[$type],
        '#default_value' => isset($collection, $collection->sources[$type]) ? $collection->sources[$type] : array(),
        '#parents' => array('sources', $type),
      );
      // Add settings for each type.
      if (isset($settings[$type])) {
        $form['sources'][$type]['settings'] = array_merge(array(
          '#tree' => TRUE,
          '#parents' => array('settings', $type),
          ), $settings[$type]);
      }
    }
  }
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['advanced']['override_base_url'] = array(
    '#type' => 'checkbox',
    '#default_value' => isset($collection, $collection->advanced['override_base_url']) ? $collection->advanced['override_base_url'] : 0,
    '#title' => t('Override base url')
  );


  $form['advanced']['base_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Override base url'),
    '#default_value' => isset($collection, $collection->advanced['base_url']) ? $collection->advanced['base_url'] : '',
    '#description' => t('Change the domain to use in search. Example: "example.com" would change @base_url/... to  http://example.com/... ', array(
      '@base_url' => $base_url,
    )),
    '#size' => 60,
    '#states' => array(
      'disabled' => array(
        ':input[name="override_base_url"]' => array('checked' => FALSE),
      ),
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Form validate handler.
 */
function fw_indexsvc_collection_form_validate($form, &$form_state) {
  // Ensure a safe machine name.
  if (!preg_match('/^[A-Za-z_][A-Za-z0-9_\-]*$/', $form_state['values']['name'])) {
    form_set_error('name', t('The collection name may only contain letters, underscores, dash, and trailing numbers.'));
  }

  // Ensure the machine name is unique.
  if (!isset($form_state['values']['iid'])) {
    $collection = fw_indexsvc_get_collection($form_state['values']['name']);
    if (!empty($collection)) {
      form_set_error('name', t('Collection names must be unique. This collection name is already in use.'));
    }
  }
}

/**
 * Form submit handler.
 */
function fw_indexsvc_collection_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $iid = isset($values['iid']) ? $values['iid'] : 0;
  fw_indexsvc_update_collection($iid, $values['name'], $values['endpoint'], $values['sources'], $values['advanced'], $values['settings']);
  $form_state['redirect'] = FW_INDEXSVC_ADMIN_PATH;
}

/**
 * Delete collection page.
 */
function fw_indexsvc_delete_confirm($form, &$form_state, $collection) {
  if (empty($collection)) {
    drupal_goto(FW_INDEXSVC_ADMIN_PATH);
  }

  $form['iid'] = array('#type' => 'value', '#value' => $collection->iid);

  return confirm_form($form,
    t('Are you sure you want to delete %title?', array(
      '%title' => $collection->name)),
    !empty($_GET['destination']) ?
      $_GET['destination'] : FW_INDEXSVC_ADMIN_PATH,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit handler for form: fw_indexsvc_delete_confirm().
 */
function fw_indexsvc_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    fw_indexsvc_delete_collection(NULL, $form_state['values']['iid']);
  }
  $form_state['redirect'] = FW_INDEXSVC_ADMIN_PATH;
}

/**
 * Upload collection page.
 */
function fw_indexsvc_reindex($form, &$form_state, $collection) {
  if (empty($collection)) {
    drupal_goto(FW_INDEXSVC_ADMIN_PATH);
  }

  $form['iid'] = array('#type' => 'value', '#value' => $collection->iid);

  return confirm_form($form,
    t('Are you sure you want to do a full upload to %title?', array(
      '%title' => $collection->name)),
    !empty($_GET['destination']) ?
      $_GET['destination'] : FW_INDEXSVC_ADMIN_PATH,
    t('This action can\'t be aborted.'),
    t('Continue'),
    t('Cancel')
  );
}

/**
 * Submit handler for form: fw_indexsvc_reindex().
 */
function fw_indexsvc_reindex_submit($form, &$form_state) {
  $collection = fw_indexsvc_get_collection(NULL, $form_state['values']['iid']);
  batch_set(fw_indexsvc_create_reindex_batch($collection));
  $form_state['redirect'] = FW_INDEXSVC_ADMIN_PATH;
}

/**
 * Clear collection page.
 */
function fw_indexsvc_clear($form, &$form_state, $collection) {
  if (empty($collection)) {
    drupal_goto(FW_INDEXSVC_ADMIN_PATH);
  }

  $form['iid'] = array('#type' => 'value', '#value' => $collection->iid);

  return confirm_form($form,
    t('Are you sure you want to clear the index on %title?', array(
      '%title' => $collection->name)),
    !empty($_GET['destination']) ?
      $_GET['destination'] : FW_INDEXSVC_ADMIN_PATH,
    t('This action can\'t be aborted.'),
    t('Continue'),
    t('Cancel')
  );
}

/**
 * Submit handler for form: fw_indexsvc_clear().
 */
function fw_indexsvc_clear_submit($form, &$form_state) {
  $collection = fw_indexsvc_get_collection(NULL, $form_state['values']['iid']);
  $collections = array(
    $collection->name => $collection,
  );
  $handlers = fw_indexsvc_get_handlers(NULL, $collections);
  // Get the unique handlers.
  $handlers = array_unique(call_user_func_array('array_merge', $handlers));
  foreach ($handlers as $handler) {
    if (method_exists($handler, 'clear')) {
      if ($handler->clear()) {
        drupal_set_message(t("The index for %name is now clearing.", array(
          '%name' => (string) $handler,
        )));
      }
    }
  }
  $form_state['redirect'] = FW_INDEXSVC_ADMIN_PATH;
}
