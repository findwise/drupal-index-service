# i3 Index Service integration for Drupal.

A Drupal module to integrate with the Index Service (**2.x**) in the i3 platform.

## Getting started

1. [Download module: **fw_indexsvc** (**2.0**)](https://bitbucket.org/findwise/drupal-index-service/get/7.x-2.0.0.tar.gz)
2. Add the module to sites/all/modules in your Drupal installation
    * Set the folder's name to **fw_indexsvc**
3. Enable module from the interface in
```Administration » Modules``` (**/admin/modules**)
4. Create an collection
    1. Navigate to
```Administration » Configuration » Search and metadata »  Index services``` (**/admin/config/search/indexservices**)
    2. From here you can use the link at the top of the list to **Add collection**.
    3. You will need a **collection name** and **endpoint url** for the index
  service that Findwise has created for you.
    4. Select the node -types and file fields you want to index.
    5. Save the collection settings.
5. If the index service is up and running it will have a status of **"Running"** in green.
6. You can now do a full-index of your site by clicking the **Upload** button.
7. If everything is OK, the number of uploaded documents should match the number selected entities found locally under the uploaded column.

## Troubleshooting


### Inaccurate total uploaded documents

Sometimes we stumble upon nodes that don't want to get indexed, usually it has been related to special characters that slip through somewhere. If the difference isn't large it will not affect your search experience, but feel free help us trace down the issue.

> Check your log for messages marked with *fw_indexsvc* under
```Reports » Recent log messages```. If any messages are found, please forward these to your Findwise contact.

### Error, connectivity issues

Typically the **endpoint URL** or **collection name** have not been entered correctly in your settings. It could also be caused by misconfigured firewall issues between your Drupal server and the Index Service.

> Check your settings if entered correctly. If you suspect firewall issues or something unknown your Findwise contact will happily help.

## Glossary

### Collection

An *collection* is used in the index service to separate different collections
of data. For example could each of your website or document storage get one
collection each. The spelling and letter casing are important as they are used
to identify where, which and what indexing process each document should go through.

## License

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA  02110-1301, USA.

-------

Copyright © 2015 Findwise AB
